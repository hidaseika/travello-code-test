//
//  FakeApiService.swift
//  TravelloCodeTestTests
//
//  Created by DevStack MacBook on 10/08/20.
//  Copyright © 2020 DevStack. All rights reserved.
//

import Foundation
import RxSwift

@testable import TravelloCodeTest

class FakeApiService : ApiService {
    private var fakeUsers: [User]
    
    init(users: [User]) {
        self.fakeUsers = users
    }
    
    override func fetchUser(completion: @escaping ([User], Bool) -> ()) {
        completion(fakeUsers, false)
    }
}
