//
//  ViewController.swift
//  TravelloCodeTest
//
//  Created by DevStack MacBook on 10/08/20.
//  Copyright © 2020 DevStack. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {

    @IBOutlet weak var userTable: UITableView!
    
    let userViewModel = UserViewModel(apiService: ApiService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userTable.register(UINib(nibName: "UserViewCell", bundle: nil), forCellReuseIdentifier: "UserViewCell")
        // fetch users data
        userViewModel.getUsers()
        
        // bind user from viewmodel to table
        userViewModel.users.bind(to: userTable.rx.items(cellIdentifier: "UserViewCell", cellType: UserViewCell.self)){
            row, user, cell in
            cell.prepareCell(user)
        }                
    }

    
}

