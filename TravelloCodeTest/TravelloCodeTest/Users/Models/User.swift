//
//  User.swift
//  TravelloCodeTest
//
//  Created by DevStack MacBook on 10/08/20.
//  Copyright © 2020 DevStack. All rights reserved.
//

import Foundation

struct User : Codable {
    let id: String
    let userId: Int
    let title: String
    let body: String
    let imageUrl: String
    let thumbnailUrl: String
}
