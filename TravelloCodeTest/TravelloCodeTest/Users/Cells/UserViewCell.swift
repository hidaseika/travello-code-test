//
//  UserViewCell.swift
//  TravelloCodeTest
//
//  Created by DevStack MacBook on 10/08/20.
//  Copyright © 2020 DevStack. All rights reserved.
//

import UIKit
import SDWebImage

class UserViewCell: UITableViewCell {

    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepareCell(_ data: User){
        userIdLabel.text = "\(data.userId)"
        titleLabel.text = data.title
        bodyTextView.text = data.body
        setupImage(userImage: data.imageUrl, userImagePlaceholder: data.thumbnailUrl)
    }
    
    private func setupImage(userImage: String, userImagePlaceholder: String){
        SDWebImageManager.shared.loadImage(with: URL(string: userImagePlaceholder), options: SDWebImageOptions(rawValue: 0), progress: nil){
            (image, _, _, _, _, _) in
            if let _ = image {
                self.userImageView?.sd_setImage(with: URL(string: userImage), placeholderImage: image)
            }
        }
    }
    
}
